#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Batch Invoice Timeline',
    'name_de_DE': 'Buchhaltung Buchungsstapel Rechnung Gültigkeitsdauer',
    'name_de_DE': '',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Timeline for Invoices on Batches
    - Adds timeline features to the Batch Invoice Module
''',
    'description_de_DE': '''Gültigkeitsdauer für Rechnungen im Buchungsstapel
    - Fügt die Merkmale der Gültigkeitsdauer-Module zu Rechnungen im
      Buchungsstapel hinzu.
''',
    'depends': [
        'account_batch_invoice',
        'account_batch_timeline',
    ],
    'xml': [],
    'translation': [
    ],
}
