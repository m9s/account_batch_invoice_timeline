# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
"account_batch_invoice_timeline"
from trytond.model import ModelView, ModelSQL
from trytond.pool import Pool


class Line(ModelSQL, ModelView):
    _name = 'account.batch.line'

    def _find_account_id_for_date(self, account_id=None, date=None):
        pool = Pool()
        account_obj = pool.get('account.account')
        fiscalyear_obj = pool.get('account.fiscalyear')
        date_obj = pool.get('ir.date')
        today = date_obj.today()
        if account_id is None:
            return -1

        if date is None:
            date = today

        fiscalyear_id = fiscalyear_obj.get_fiscalyear_id(date)
        all_account_ids = [account_id]
        all_account_ids.extend(account_obj._get_all_predecessors(account_id))
        all_account_ids.extend(account_obj._get_all_successors(account_id))
        new_account_ids = account_obj.search([
                        ('id', 'in', all_account_ids),
                        ('fiscalyear', '=', fiscalyear_id),
                ])
        if not new_account_ids:
            return -1

        new_account_id = new_account_ids[0]
        return new_account_id

    def _get_invoice_account_id(self, invoice_id, date):
        new_invoice_id = self._find_account_id_for_date(invoice_id, date)

        return new_invoice_id
Line()
